# Maya-PythonPackages (and DISCLAIMER)

This repository reserves Maya-Python compatible packages.  
The version of Maya-Python (MayaPy) is **2.7.11**, and compiled with **MSC v.1900 64 bit (AMD64)**.  
For the compatibility, all wheels in this repository are also compiled with **Visual Studio 2015 (x64)**.  

Until the installation, you may get warning on using **Python2** continously, but just ignore them.  
It is none of our fault! :)

## Tested Environment

- Windows 10 (x64)  
- **CUDA 9.0** supported NVidia GPU.  
- Maya installation path is default.  

## Installation

### 0. prepare pip for MayaPy

First, you want to run command line with **admistrative mode**.  
Then you want to run the following lines for the environment path.  
```sh
> path=%PATH%;"C:\Program Files\Autodesk\Maya2018\bin";"C:\Program Files\Autodesk\Maya2018\Python\Scripts"
```

### 1. get and upgrade some installers for MayaPy

- run **get-pip.py**. You need to **download** before running.  
  You can get the original file from [https://bootstrap.pypa.io/get-pip.py](https://bootstrap.pypa.io/get-pip.py).
```sh
> mayapy get-pip.py
```  
- ~~upgrade **pip** and **setuptools**.~~ Maybe, you don't need this process due to Python 2.7 deprecating ...
```sh
> pip install --upgrade pip setuptools
```  

### 2. Install custom-built wheels

- Once you install and upgrade **pip**, now move on custom-built wheel installation.  
- Watch out your command. Do not install the default wheels through remote repository. Those wheels are not compatible with our testbed (and MayaPy).  
- Run the following codes to install mandatory packages (**Cython**,**numpy**,**h5py**,**scipy**):  
```sh
> pip install https://bitbucket.org/stnoh/Maya-PythonPackages/raw/master/Cython-0.29-cp27-cp27m-win_amd64.whl
> pip install https://bitbucket.org/stnoh/Maya-PythonPackages/raw/master/numpy-1.14.5+mkl-cp27-cp27m-win_amd64.whl
> pip install https://bitbucket.org/stnoh/Maya-PythonPackages/raw/master/h5py-2.8.0-cp27-cp27m-win_amd64.whl
> pip install https://bitbucket.org/stnoh/Maya-PythonPackages/raw/master/scipy-1.1.0+mkl-cp27-cp27m-win_amd64.whl
```  
- If you want to use scikit-optimize, you also need to install **scikit-learn** in advance:  
```sh
> pip install https://bitbucket.org/stnoh/Maya-PythonPackages/raw/master/scikit_learn-0.20.2-cp27-cp27m-win_amd64.whl
```  

### 3. Install tensorflow by custom-built wheel

Now you can install tensorflow.  
```sh
> pip install https://bitbucket.org/stnoh/Maya-PythonPackages/raw/master/tensorflow-1.11.0+mkl-cp27-cp27m-win_amd64.whl
```

#### Alternative: CNTK

You can alternatively use [cntk](https://docs.microsoft.com/en-us/cognitive-toolkit/).  
```sh
> pip install cntk cntk-gpu
```

If you want to use CNTK in keras, you need to change keras setting file at:
```sh
C:/Users/{your account}/Documents/.keras/keras.json
```
Change **backend** from **tensorflow** to **cntk**. You may need to restart Maya.  
Please note that this is different with the original keras path, which is located under **"C:/Users/{your account}/.keras"** as default.  


### 4. Install other VS2015-compatible packages

These are not necessary to run tensorflow, but quite helpful packages. I recommend you to install below packages, too.  
```sh
> pip install keras==2.2.5
> pip install scikit-optimize
```

**[CAUTION!]** OpenCV-4 (installed by **opencv-python**) has severe speed issue in highgui, so I recommend you to use cv2.pyd in OpenCV2 package.  
Copy **cv2.pyd** (it is also included in this repository) to the following directory (it need administrative authority):  
```sh
C:/Program Files/Autodesk/Maya2018/Python/Lib/site-packages
```

If you want to use video rendering, then you need **ffmpeg**. Please note that this specific module is under **LGPL**.  
Copy **ffmpeg/opencv_ffmpeg2413_64.dll** to the following directory (it need administrative authority):
```sh
C:/Program Files/Autodesk/Maya2018/bin
```



### ~~Miscellaneous~~

The following packages are not used anymore.  
I just keep them for the records.  

- **theano**  
  This is an obsolete deep learning package. I do not recommend you to use this.  
