# Model

## Normalized VGG-19 for keras

This is just a memorandum for the reproducibility.  

1. Get the original data from bethgelab:  
   - caffe model: https://github.com/leongatys/DeepTextures/blob/master/Models/VGG_ave_pool_deploy.prototxt
   - normalized weights (about 78MB): http://bethgelab.org/media/uploads/deeptextures/vgg_normalised.caffemodel 
2. Update prototxt version to caffe2.
   > patch VGG_ave_pool_deploy.prototxt VGG_ave_pool_deploy.prototxt.patch -o VGG_ave_pool_deploy_patched.prototxt  
3. Convert them using [caffe2keras](https://github.com/qxcv/caffe2keras).
   > python -m caffe2keras VGG_ave_pool_deploy_patched.prototxt vgg_normalised.caffemodel vgg19_normalized_notop.h5
4. Now you have **vgg19_normalized_notop.h5**.  
